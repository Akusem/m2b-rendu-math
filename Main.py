import os

print("Launch Python scripts")
os.system("python scripts/Comparaison_modele.py")
os.system("python scripts/Knn.py")
os.system("python scripts/Knn_different_algorithms.py")
os.system("python scripts/Knn_different_neighbors.py")
os.system("python scripts/create_model.py")
os.system("python scripts/linearSVC.py")
os.system("python scripts/linearSVC_different_penality.py")
os.system("python scripts/random_forest.py")
os.system("python scripts/Knn_f-score.py")
print("Launch R script")
os.system("R CMD BATCH scripts/PCA.R")

