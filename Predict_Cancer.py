import pickle
import argparse
import numpy as np
import pandas as pd 
from sklearn import neighbors
from sklearn.feature_selection import SelectKBest, chi2, f_classif
from sklearn.metrics import accuracy_score

parser = argparse.ArgumentParser(description="Un programme servant à prédire entre 5 type de cancer")
parser.add_argument("data", type=str, help="Nom d'un fichier csv contenant soit les données des 20250 gènes, soit des 12 gènes")
parser.add_argument("output", type=str, help="Nom du fichier où son écrit les prédictions au format tsv")
args = parser.parse_args()

# Lis le fichier de data fournis
data = pd.read_csv(args.data)
data = data.iloc[:,2:]

# Si le tableau fourni contient les mêmes gènes que le jeux de données de test, procède à la feature selection 
if data.shape[1] == 20530:
    # Charge l'objet de feature selection
    fts = pickle.load(open("save_model/knn_uniform_12_feature_selection", 'rb'))
    data = fts.transform(data)

# Charge le modèle
clf = pickle.load(open("save_model/knn_uniform_12_model", 'rb'))

# Procède à la prédiction
prediction = clf.predict(data)

# Ouvre le fichier d'output
output = open(args.output, "w")
# Print et enregistre les predictions
for i in range(len(prediction)):
    print(f"Patient {i}: {prediction[i]}")
    output.write(f"Patient {i},{prediction[i]}\n")
