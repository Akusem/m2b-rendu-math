# M2B: Rendu du projet de Math

Vous trouverez ici les scripts utilisés pour notre analyse ainsi 
que le programme permettant de determiner le type de cancer d'un patient (parmi
les types de cancers étudiés) à partir de l'expression de ses gènes.

## Environnement Conda

Afin de faciliter la reproductibilité de notre analyse,
l'environment conda de développement est fourni sous forme de fichier yml. 
Vous pouvez le recréer avec la commande suivante:

```
conda env create -n nom_de_votre_environment -f conda_env.yml
```

## Programme de prediction du type de cancer

Le programme Predict_Cancer.py utilise un modèle KNN entrainé sur l'expression génétique
de 802 individus atteints de 5 types de cancers différents :
BRCA, KIRC, LUAD, PRAD et COAD.
Il prend en entrée un tableau au format csv contenant des données de RNA-seq (soit l'expression
des gènes) avec 20.530 gène ordonnés comme dans notre analyse, ou plus simplement
avec les 12 gènes d'intêret :
Gene_218    Gene_219    Gene_449    Gene_3736   Gene_3920   Gene_7963
Gene_9174   Gene_9175   Gene_13817  Gene_14113  Gene_15894  Gene_18134

Le modèle utilisé dans Predict_Cancer.py est créé par create_model.py situé dans le dossier script.

**Utilisation:**

```
python Predict_Cancer.py data.csv output_file.csv
```

**Aide:**
```
usage: Predict_Cancer.py [-h] data output

Un programme servant à prédire entre 5 type de cancer

positional arguments:
  data        Nom d'un fichier csv contenant soit les données des 20250 gènes,
              soit des 12 gènes
  output      Nom du fichier où son écrit les prédictions au format tsv

optional arguments:
  -h, --help  show this help message and exit
```

## Scripts de representation graphique des résultats

Les scripts utilisés pour notre étude sont fournis dans le dossier scripts/. 
Un script nommé Main.py est présent à la racine pour exécuter toute l'analyse.

IMPORTANT : Les données à étudier **doivent** se trouver dans le dossier data/, sous 
la forme :  data.csv    labels.csv

```
python Main.py
```
