import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
from sklearn import neighbors, datasets, svm
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.feature_selection import SelectKBest, chi2, f_classif, mutual_info_classif
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split



def find_gene(support):
    gene_interet = []
    i = 0
    print("Gene d'intêret: ", end='')
    support = support.tolist()
    for gene in support:
        if gene == True:
            gene_interet.append(i)
            print(f"Gene_{i}", end=', ')
        i += 1
    
    print('\n')


# Read the file
data = pd.read_csv("data/data.csv")
data = data.iloc[:,2:]
label = pd.read_csv("data/labels.csv")
label = label.iloc[:,1]

training_accuracy, testing_accuracy = [], []
svc_training_accuracy, svc_testing_accuracy = [], []
ksvm_training_accuracy, ksvm_testing_accuracy = [], []
lr_training_accuracy, lr_testing_accuracy = [], []
gnb_training_accuracy, gnb_testing_accuracy = [], []
dtc_training_accuracy, dtc_testing_accuracy = [], []
rf_training_accuracy, rf_testing_accuracy = [], []

find_knn, find_svc = False, False

# Search for the best number of feature to get the best accuracy
for i in range(1, len(data.iloc[1,:])):

    # Feature selection
    fts = SelectKBest(f_classif, k=i).fit(data, label) #ch2, f_classif, mutual_info_classif 
    # Obtention d'un numpy.ndarray contenant sous forme de booléen les positions des 
    # colonnes sélectionnées.
    support = fts.get_support()
    print("nb feature: ", i)
    find_gene(support)
    # Filtre les features d'intêret 
    new_data = fts.transform(data)
    print(new_data.shape)
    # Divise le jeux de données à 67% de données de training et 33% de test
    data_train, data_test, label_train, label_test = train_test_split(new_data, label, test_size=0.33)
    
    # Créer l'objet KNeighborsClassifier et l'entraine avec les données de training
    n_neighbors = 10
    clf = neighbors.KNeighborsClassifier(n_neighbors, weights='uniform')
    clf.fit(data_train, label_train)

    # Créer le modèle linearSVC et l'entraine
    c = 1.0
    svc = svm.SVC(C=c, kernel='linear')
    svc.fit(data_train, label_train)

    # kernelSVM
    ksvm = svm.SVC(kernel = 'rbf', random_state = 0)
    ksvm.fit(data_train, label_train)

    # Logistic regression
    lr = LogisticRegression(random_state = 0)
    lr.fit(data_train, label_train)

    # Gaussian NB
    gnb = GaussianNB()
    gnb.fit(data_train, label_train)

    # DecisionTreeClassifier
    dtc = DecisionTreeClassifier(criterion = 'entropy', random_state = 0)
    dtc.fit(data_train, label_train)

    # Random forest
    rf = RandomForestClassifier(n_estimators = 10, criterion = 'entropy', random_state = 0)
    rf.fit(data_train, label_train)

    # Teste l'accuracy du modèle knn
    training_accuracy.append(accuracy_score(clf.predict(data_train),label_train))
    testing_accuracy.append(accuracy_score(clf.predict(data_test),label_test))

    # Teste l'accuracy du modèle linearSVC
    svc_training_accuracy.append(accuracy_score(svc.predict(data_train),label_train))
    svc_testing_accuracy.append(accuracy_score(svc.predict(data_test),label_test))
    
    # Teste l'accuracy du modèle linearSVC
    ksvm_training_accuracy.append(accuracy_score(ksvm.predict(data_train),label_train))
    ksvm_testing_accuracy.append(accuracy_score(ksvm.predict(data_test),label_test))

    # Teste l'accuracy du modèle linearSVC
    lr_training_accuracy.append(accuracy_score(lr.predict(data_train),label_train))
    lr_testing_accuracy.append(accuracy_score(lr.predict(data_test),label_test))

    # Teste l'accuracy du modèle linearSVC
    gnb_training_accuracy.append(accuracy_score(gnb.predict(data_train),label_train))
    gnb_testing_accuracy.append(accuracy_score(gnb.predict(data_test),label_test))

    # Teste l'accuracy du modèle linearSVC
    dtc_training_accuracy.append(accuracy_score(dtc.predict(data_train),label_train))
    dtc_testing_accuracy.append(accuracy_score(dtc.predict(data_test),label_test))

    # Teste l'accuracy du modèle linearSVC
    rf_training_accuracy.append(accuracy_score(rf.predict(data_train),label_train))
    rf_testing_accuracy.append(accuracy_score(rf.predict(data_test),label_test))
    
    # Affiche cette accuracy
    print("training accuracy:", training_accuracy[i-1], "Testing accuracy:", testing_accuracy[i-1])
    print("svc_training accuracy:", svc_training_accuracy[i-1], "svc_Testing accuracy:", svc_testing_accuracy[i-1])
    print("ksvm_training accuracy:", ksvm_training_accuracy[i-1], "ksvm_Testing accuracy:", ksvm_testing_accuracy[i-1])
    print("lr_training accuracy:", lr_training_accuracy[i-1], "lr_Testing accuracy:", lr_testing_accuracy[i-1])
    print("gnb_training accuracy:", gnb_training_accuracy[i-1], "gnb_Testing accuracy:", gnb_testing_accuracy[i-1])
    print("dtc_training accuracy:", dtc_training_accuracy[i-1], "dtc_Testing accuracy:", dtc_testing_accuracy[i-1])
    print("rf_training accuracy:", rf_training_accuracy[i-1], "rf_Testing accuracy:", rf_testing_accuracy[i-1])

    z = clf.predict(data_test)
    print("crosstab:\n", pd.crosstab(label_test, z))

    if i == 31:
        break
    # Si l'accuracy est égale à 100% pour les 2 modèle interompt les tests et fait le plot
    # if testing_accuracy[i-1] == 1.0:
    #     find_knn = True
    # if svc_testing_accuracy[i-1] == 1.0:
    #     find_svc = True
    # if find_knn == True and find_svc == True:
    #     break

print("training accuracy:", training_accuracy[-2], "Testing accuracy:", testing_accuracy[-2])
print("svc_training accuracy:", svc_training_accuracy[-2], "svc_Testing accuracy:", svc_testing_accuracy[-2])
print("ksvm_training accuracy:", ksvm_training_accuracy[-2], "ksvm_Testing accuracy:", ksvm_testing_accuracy[-2])
print("lr_training accuracy:", lr_training_accuracy[-2], "lr_Testing accuracy:", lr_testing_accuracy[-2])
print("gnb_training accuracy:", gnb_training_accuracy[-2], "gnb_Testing accuracy:", gnb_testing_accuracy[-2])
print("dtc_training accuracy:", dtc_training_accuracy[-2], "dtc_Testing accuracy:", dtc_testing_accuracy[-2])
print("rf_training accuracy:", rf_training_accuracy[-2], "rf_Testing accuracy:", rf_testing_accuracy[-2])


fig, ax = plt.subplots()
color = 'tab:blue'
ax.set_xlabel('Nombre de feature')
ax.set_ylabel('Testing accuracy des modèles')
#ax.plot(range(len(training_accuracy)), training_accuracy, color=color, label="KNN Training accuracy")
color = 'tab:green'
ax.plot(range(len(training_accuracy)), testing_accuracy, color=color, label="KNN Testing accuracy")

#ax.plot(range(len(training_accuracy)), svc_training_accuracy, color='#ff6400', label="Linear_SVC Training accuracy")
ax.plot(range(len(training_accuracy)), svc_testing_accuracy, color='tab:red', label="Linear_SVC Testing accuracy")
ax.plot(range(len(training_accuracy)), ksvm_testing_accuracy, color='#ff6400', label="Kernel SVM Testing accuracy")
ax.plot(range(len(training_accuracy)), lr_testing_accuracy, color='tab:blue', label="Logistic Regression Testing accuracy")
ax.plot(range(len(training_accuracy)), gnb_testing_accuracy, color='#b72e84', label="GaussianNB Testing accuracy")
ax.plot(range(len(training_accuracy)), dtc_testing_accuracy, color='#ffe100', label="DecisionTreeClassifier Testing accuracy")
ax.plot(range(len(training_accuracy)), rf_testing_accuracy, color='#000000', label="Random Forest Testing accuracy")


ax.set_xticks(np.arange(0, len(training_accuracy), 2))
ax.set_yticks(np.arange(0.5, 1.005, 0.02))

plt.title("Learning curve")
fig.tight_layout()
plt.grid(True)
plt.legend()
plt.savefig(f"output/comparaison_modele_all_f_classif.png", format="png", dpi=1300)
plt.show()

