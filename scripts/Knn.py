import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
from sklearn import neighbors
from sklearn.feature_selection import SelectKBest, chi2, f_classif
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

# Read the file
data = pd.read_csv("data/data.csv")
data = data.iloc[:,2:]
label = pd.read_csv("data/labels.csv")
label = label.iloc[:,1]

training_accuracy, testing_accuracy = [], []

# Search for the best number of feature to get the best accuracy
for i in range(1, len(data.iloc[1,:])):

    # Feature selection
    fts = SelectKBest(f_classif, k=i).fit(data, label)
    # Obtention d'un numpy.ndarray contenant sous forme de booléen les positions des 
    # colonnes sélectionnées.
    support = fts.get_support()  
    # Filtre les features d'intêret 
    new_data = fts.transform(data)
    # Divise le jeux de données à 67% de données de training et 33% de test
    data_train, data_test, label_train, label_test = train_test_split(new_data, label, test_size=0.33, random_state=13)
    
    # Créer l'objet KNeighborsClassifier et l'entraine avec les données de training
    n_neighbors = 10
    clf = neighbors.KNeighborsClassifier(n_neighbors, weights='uniform')
    clf.fit(data_train, label_train)

    clf.fit(data_train, label_train)
    # Teste l'accuracy du modèle
    training_accuracy.append(accuracy_score(clf.predict(data_train),label_train))
    testing_accuracy.append(accuracy_score(clf.predict(data_test),label_test))

    # Affiche cette accuracy
    print("training accuracy:", training_accuracy[i-1], "Testing accuracy:", testing_accuracy[i-1])
    z = clf.predict(data_test)
    print("crosstab:\n", pd.crosstab(label_test, z))

    if i == 21:
        break

fig, ax = plt.subplots()
ax.set_xlabel('Nombre de feature')
ax.set_ylabel('Training and Testing accuracy')
ax.plot(range(len(training_accuracy)), training_accuracy, color='tab:blue', label="Training accuracy")
ax.plot(range(len(training_accuracy)), testing_accuracy, color='tab:green', label="Testing accuracy")
ax.set_xticks(np.arange(0, len(training_accuracy), 2))
ax.set_yticks(np.arange(0.5, 1.005, 0.05))

plt.title("Learning curve")
fig.tight_layout()
plt.grid(True)
plt.legend()
plt.savefig(f"output/Knn_learning_curve_uniform.png", format="png", dpi=1300)
plt.show()
