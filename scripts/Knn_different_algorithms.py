import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
from sklearn import neighbors
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split


data = pd.read_csv("data/data.csv")
data = data.iloc[:,2:]
label = pd.read_csv("data/labels.csv")
label = label.iloc[:,1]

training_accuracy, testing_accuracy = [], []
clfk_training_accuracy, clfk_testing_accuracy = [], []
clfbr_training_accuracy, clfbr_testing_accuracy = [], []

for i in range(1, len(data.iloc[1,:])):
        # Feature selection
        fts = SelectKBest(f_classif, k=i).fit(data, label)
        # Obtention d'un numpy.ndarray contenant sous forme de booléen les positions des 
        # colonnes sélectionnées.
        support = fts.get_support()  
        # Filtre les features d'intêret 
        new_data = fts.transform(data)
        # Divise le jeux de données à 67% de données de training et 33% de test
        data_train, data_test, label_train, label_test = train_test_split(new_data, label, test_size=0.33)

        #Créer l'objet KNeighborsClassifier et l'entraine avec les données de training
        n_neighbors = 10
        clfba = neighbors.KNeighborsClassifier(n_neighbors, weights='uniform', algorithm='ball_tree')
        clfk = neighbors.KNeighborsClassifier(n_neighbors, weights='uniform', algorithm='kd_tree')
        clfbr = neighbors.KNeighborsClassifier(n_neighbors, weights='uniform', algorithm='brute')

        clfba.fit(data_train, label_train)
        clfk.fit(data_train, label_train)
        clfbr.fit(data_train, label_train)

        # Teste l'accuracy du modèle
        training_accuracy.append(accuracy_score(clfba.predict(data_train),label_train))
        testing_accuracy.append(accuracy_score(clfba.predict(data_test),label_test))

        clfk_training_accuracy.append(accuracy_score(clfk.predict(data_train),label_train))
        clfk_testing_accuracy.append(accuracy_score(clfk.predict(data_test),label_test))

        clfbr_training_accuracy.append(accuracy_score(clfbr.predict(data_train),label_train))
        clfbr_testing_accuracy.append(accuracy_score(clfbr.predict(data_test),label_test))

        # Affiche cette accuracy
        print("training accuracy:", training_accuracy[i-1], "Testing accuracy:", testing_accuracy[i-1])
        print("training accuracy:", clfk_training_accuracy[i-1], "Testing accuracy:", clfk_testing_accuracy[i-1])
        print("training accuracy:", clfbr_training_accuracy[i-1], "Testing accuracy:", clfbr_testing_accuracy[i-1])

        z = clfba.predict(data_test)
        print("crosstab:\n", pd.crosstab(label_test, z))

        if training_accuracy[i-1] == 0:
                break
        elif i == 50:
                break


fig, ax = plt.subplots()
color = 'tab:blue'
ax.set_xlabel('Nombre de features')
ax.set_ylabel('Training and Testing accuracy')
color = 'tab:green'
ax.plot(range(len(training_accuracy)), testing_accuracy, color="tab:blue", label="ball_tree Testing accuracy")
ax.plot(range(len(training_accuracy)), clfk_testing_accuracy, color="tab:green", label="kd_tree Testing accuracy")
ax.plot(range(len(training_accuracy)), clfbr_testing_accuracy, color="tab:red", label="brute Testing accuracy")

ax.set_xticks(np.arange(0, len(training_accuracy), 2))
ax.set_yticks(np.arange(0.5, 1.005, 0.05))

plt.title("Learning curve avec 3 algorithmes de KNN comparé")
fig.tight_layout()
plt.grid(True)
plt.legend()
plt.savefig(f"output/Knn_test_different_algorithms.png", format="png", dpi=1300)
plt.show()
