import pickle
import numpy as np
import pandas as pd 
from sklearn import neighbors
from sklearn.feature_selection import SelectKBest, chi2, f_classif
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

# Read the file
data = pd.read_csv("data/data.csv")
data = data.iloc[:,2:]
label = pd.read_csv("data/labels.csv")
label = label.iloc[:,1]

# Feature selection
fts = SelectKBest(f_classif, k=12).fit(data, label)
# Obtention d'un numpy.ndarray contenant sous forme de booléen les positions des 
# colonnes sélectionnées.
support = fts.get_support()  
# Filtre les features d'intêret 
new_data = fts.transform(data)
# Divise le jeux de données à 67% de données de training et 33% de test
data_train, data_test, label_train, label_test = train_test_split(new_data, label, test_size=0.33, random_state=13)

# Créer l'objet KNeighborsClassifier et l'entraine avec les données de training
n_neighbors = 10
clf = neighbors.KNeighborsClassifier(n_neighbors, weights='uniform')
clf.fit(data_train, label_train)

clf.fit(data_train, label_train)

# Teste l'accuracy du modèle
training_accuracy = accuracy_score(clf.predict(data_train),label_train)
testing_accuracy = accuracy_score(clf.predict(data_test),label_test)

# Affiche cette accuracy
print("training accuracy:", training_accuracy, "Testing accuracy:", testing_accuracy)
z = clf.predict(data_test)
print("crosstab:\n", pd.crosstab(label_test, z))

pickle.dump(fts, open('save_model/knn_uniform_12_feature_selection', 'wb'))
pickle.dump(clf, open('save_model/knn_uniform_12_model', 'wb'))