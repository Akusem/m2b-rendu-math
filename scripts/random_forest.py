import numpy as np
import pandas as pd 
from subprocess import call
from sklearn.tree import export_graphviz
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest, chi2, f_classif

data = pd.read_csv("data/data.csv")
data = data.iloc[:,2:]
label = pd.read_csv("data/labels.csv")
label = label.iloc[:,1]

# Feature selection
fts = SelectKBest(f_classif, k=12).fit(data, label)
# Obtention d'un numpy.ndarray contenant sous forme de booléen les positions des 
# colonnes sélectionnées.
support = fts.get_support()  
# Filtre les features d'intêret 
new_data = fts.transform(data)
# Divise le jeux de données à 67% de données de training et 33% de test
data_train, data_test, label_train, label_test = train_test_split(new_data, label, test_size=0.33, random_state=13)

model = RandomForestClassifier(n_estimators=10)
model.fit(data_train, label_train)

label_names = np.array(['BRCA', 'COAD', 'KIRC', 'LUAD', 'PRAD'])

data_names = np.array(["Gene_"+str(i) for i in range(new_data.shape[1])])


estimator = model.estimators_[5]
export_graphviz(estimator, out_file='output/tree.dot', 
                feature_names = data_names,
                class_names = label_names,
                rounded = True, proportion = False,
                precision = 2, filled = True)

call(['dot', '-Tpng', 'output/tree.dot', '-o', 'tree.png', '-Gdpi=1000'])

print(accuracy_score(model.predict(data_test), label_test))